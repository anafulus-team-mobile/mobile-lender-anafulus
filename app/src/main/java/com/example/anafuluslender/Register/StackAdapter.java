package com.example.anafuluslender.Register;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.anafuluslender.R;

import java.util.ArrayList;
import java.util.List;

//public class StackAdapter extends ArrayAdapter<StackItems> {

//    private List<StackItems> items;
//    private Context context;
//
//    public StackAdapter(Context context, int textViewResourceId, List<StackItems> objects) {
//        super(context, textViewResourceId, objects);
//        this.items = objects;
//        this.context = context;
//    }
//
//    @NonNull
//    @SuppressLint("InflateParams")
//    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
//        ViewHolder holder; //initialize a null ViewHolder object
//        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        // If holder not exist then locate all view from UI file
//        if (convertView == null) {
//            // inflate UI from XML file
//            convertView = inflater.inflate(R.layout.item_registration_step_three, parent, false);
//            // get all UI view
//            holder = new ViewHolder(convertView);
//            // set tag for holder
//            convertView.setTag(holder);
//        } else {
//            // if holder created, get tag from view
//            holder = (ViewHolder) convertView.getTag();
//        }
//        StackItems stackItem = items.get(position);
//
//        //set stack item data to views
//        holder.image.setImageResource(stackItem.getDrawableId());
//        holder.imageName.setText(stackItem.getImageName());
//
//        return convertView;
//    }
//
//    private class ViewHolder {
//        private TextView imageName;
//        private ImageView image;
//
//        public ViewHolder(View view) {
//            this.image = (ImageView) view.findViewById(R.id.stackLayout);
//            this.imageName = (TextView) view.findViewById(R.id.textView);
//        }
//    }
//}

//    ArrayList arrayList;
//    LayoutInflater inflater;
//    ViewHolder holder = null;
//
//    public StackAdapter (Context context, ArrayList arrayList){
//        this.arrayList = arrayList;
//        this.inflater = LayoutInflater.from(context);
//    }
//
//    @Override
//    public int getCount() {
//        return arrayList.size();
//    }
//
//    @Override
//    public StackItems getItem(int position) {
//        return arrayList.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(int position, View view, ViewGroup parent) {
//        if (view == null){
//            view = inflater.inflate(R.layout.item_registration_step_three, parent, false);
//            holder = new ViewHolder();
//            holder.image = (ImageView) view.findViewById(R.id.stackLayout);
//            view.setTag(holder);
//        } else {
//            holder = (ViewHolder) view.getTag();
//        }
////        Log.d("ampas", arrayList.get(position).getLayout().toString());
//        holder.image.setBackgroundResource(arrayList.get(position).getImage());
//        return view;
//    }
//
//    public class ViewHolder {
//        ImageView image;
//    }
//}
