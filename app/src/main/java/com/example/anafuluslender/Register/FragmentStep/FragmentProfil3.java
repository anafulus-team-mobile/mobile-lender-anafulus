package com.example.anafuluslender.Register.FragmentStep;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.anafuluslender.R;

public class FragmentProfil3 extends Fragment implements View.OnTouchListener, ViewTreeObserver.OnScrollChangedListener{

    private ScrollView scrollView;

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_profil3, container, false);

        scrollView = view.findViewById(R.id.layoutProfil3);
        scrollView.setOnTouchListener(this);
        scrollView.getViewTreeObserver().addOnScrollChangedListener(this);
//        scrollUpAndDown();

        return view;
    }

//    private void scrollUpAndDown() {
//        scrollView.getViewTreeObserver()
//                .addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//                    @Override
//                    public void onScrollChanged() {
//                        if (scrollView.getChildAt(0).getBottom()
//                                == (scrollView.getHeight() + scrollView.getScrollY())) {
//                            //AT BOTTOM
//                            Toast.makeText(getContext(), "dibawah", Toast.LENGTH_SHORT).show();
//                        } else {
//                            //AT TOP
//                            Toast.makeText(getContext(), "diatas", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public void onScrollChanged() {
        View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
        int topDetector = scrollView.getScrollY();
        int bottomDetector = view.getBottom() -  (scrollView.getHeight() + scrollView.getScrollY());

        if(bottomDetector <= 0 ){
            Toast.makeText(getActivity(),"Scroll View bottom reached",Toast.LENGTH_SHORT).show();
        }
        if(topDetector <= 0){
            Toast.makeText(getActivity(),"Scroll View top reached",Toast.LENGTH_SHORT).show();
        }
    }
}
