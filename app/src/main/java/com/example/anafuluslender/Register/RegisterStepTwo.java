package com.example.anafuluslender.Register;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.anafuluslender.Login.Login;
import com.example.anafuluslender.R;
import com.goodiebag.pinview.Pinview;

public class RegisterStepTwo extends AppCompatActivity {

    ImageView buttonBack;
    Pinview pinview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_step_two);

        buttonBack = findViewById(R.id.buttonBack);
        pinview = findViewById(R.id.otp);

        //BUTTON BACK ON TOOLBAR
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterStepTwo.this, Register.class);
                startActivity(i);
                finish();
            }
        });

        //OTP
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                showPopUpSuccessVerification();
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                Toast.makeText(RegisterStepTwo.this, pinview.getValue(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(RegisterStepTwo.this, Register.class);
        startActivity(i);
        finish();
    }

    private void showPopUpSuccessVerification() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterStepTwo.this, R.style.CustomAlertDialog);
        View view = getLayoutInflater().inflate(R.layout.popup_verification_phone, null);
        builder.setCancelable(false);
        builder.setView(view);
        Button buttonOK = view.findViewById(R.id.buttonOK);
        final AlertDialog popup_verification = builder.create();


        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterStepTwo.this, RegisterStepThree.class);
                startActivity(i);
                finish();
            }
        });
        popup_verification.show();
    }
}
