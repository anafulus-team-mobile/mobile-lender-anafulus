package com.example.anafuluslender.Register.FragmentStep;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.anafuluslender.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FragmentProfil1 extends Fragment {

    private LocalBroadcastManager broadcaster;
    ScrollView scrollView;
    Spinner spinnerCountry, spinnerType;

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_profil1, container, false);

        scrollView = view.findViewById(R.id.layoutProfil1);
        spinnerCountry = view.findViewById(R.id.spinnerCountry);
        spinnerType = view.findViewById(R.id.spinnerType);
//        scrollView.setOnTouchListener(this);
//        scrollView.getViewTreeObserver().addOnScrollChangedListener(this);
        broadcaster = LocalBroadcastManager.getInstance(getActivity());
//        scrollUpAndDown();


        // Initializing a String Array
        String[] country = new String[]{
                "Kewarganegaraan",
                "Warga Negara Asing",
                "Warga Negara Indonesia"
        };

        final List<String> countryList = new ArrayList<>(Arrays.asList(country));
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, countryList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(getResources().getColor(R.color.black));
                }
                return view;
            }
        };

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerCountry.setAdapter(spinnerArrayAdapter);
        spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
//                if (position != 0){
//                    Toast.makeText(ChangePersonal.this, selectedItemText +" dipilih", Toast.LENGTH_SHORT).show();
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // Initializing a String Array
        String[] type = new String[]{
                "Tipe Pendanaan",
                "Institusi / Perusahaan",
                "Individu"
        };

        final List<String> typeList = new ArrayList<>(Arrays.asList(type));
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, typeList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(getResources().getColor(R.color.black));
                }
                return view;
            }
        };

        spinnerArrayAdapter2.setDropDownViewResource(R.layout.spinner_item);
        spinnerType.setAdapter(spinnerArrayAdapter2);
        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                if (position == 0){
                    Intent intent = new Intent("onBottom");
                            intent.putExtra("isBottom", "0");
                            broadcaster.sendBroadcast(intent);
                } else {
                    Intent intent = new Intent("onBottom");
                            intent.putExtra("isBottom", "1");
                            broadcaster.sendBroadcast(intent);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

//    @Override
//    public void onScrollChanged() {
//        View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
//        int topDetector = scrollView.getScrollY();
//        int bottomDetector = view.getBottom() -  (scrollView.getHeight() + scrollView.getScrollY());
//
//        if(bottomDetector <= 0 ){
//            Toast.makeText(getActivity(),"Scroll View bottom reached",Toast.LENGTH_SHORT).show();
//        }
//        if(topDetector <= 0){
//            Toast.makeText(getActivity(),"Scroll View top reached",Toast.LENGTH_SHORT).show();
//        }
//    }


    private void scrollUpAndDown() {
        scrollView.getViewTreeObserver()
                .addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                    @Override
                    public void onScrollChanged() {
                        if (scrollView.getChildAt(0).getBottom()
                                <= (scrollView.getHeight() + scrollView.getScrollY())) {
                            //AT BOTTOM
//                            Intent intent = new Intent("onBottom");
//                            intent.putExtra("isBottom", "1");
//                            broadcaster.sendBroadcast(intent);
                            Toast.makeText(getContext(), "dibawah", Toast.LENGTH_SHORT).show();
                        } else {
                            //AT TOP
//                            Intent intent = new Intent("onBottom");
//                            intent.putExtra("isBottom", "0");
//                            broadcaster.sendBroadcast(intent);
                            Toast.makeText(getContext(), "diatas", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

//    @Override
//    public boolean onTouch(View v, MotionEvent event) {
//        return false;
//    }
}
