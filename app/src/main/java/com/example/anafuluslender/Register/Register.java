package com.example.anafuluslender.Register;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.anafuluslender.Login.Login;
import com.example.anafuluslender.R;

public class Register extends AppCompatActivity {

    ImageView buttonBack;
    TextView textPrivacyPolicy;
    Button buttonNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        buttonBack = findViewById(R.id.buttonBack);
        textPrivacyPolicy = findViewById(R.id.textCheckBox);
        buttonNext = findViewById(R.id.buttonNext);


        //BUTTON BACK ON TOOLBAR
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Register.this, Login.class);
                startActivity(i);
                finish();
            }
        });

        //BUTTON NEXT STEP
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Register.this, RegisterStepTwo.class);
                startActivity(i);
                finish();
            }
        });

        //TEXT PRIVACY & POLICY
        final String PrivacyPolicy = "Dengan ini saya telah membaca dan setuju dengan kebijakan dan privasi dari Anafulus.";
        SpannableString ss = new SpannableString(PrivacyPolicy);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Intent i = new Intent(Register.this, com.example.anafuluslender.PrivacyPolicy.PrivacyPolicy.class);
                startActivity(i);
                finish();
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.white));
            }
        };

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
               Intent i = new Intent(Register.this, com.example.anafuluslender.PrivacyPolicy.PrivacyPolicy.class);
               startActivity(i);
               finish();
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.white));
            }
        };
        ss.setSpan(clickableSpan, 48, 57, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan1, 62, 69, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textPrivacyPolicy.setText(ss);
        textPrivacyPolicy.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(Register.this, Login.class);
        startActivity(i);
        finish();
    }
}
