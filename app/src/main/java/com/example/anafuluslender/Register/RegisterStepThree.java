package com.example.anafuluslender.Register;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.StackView;

import com.example.anafuluslender.R;
import com.example.anafuluslender.Register.FragmentStep.FragmentProfil1;
import com.example.anafuluslender.Register.FragmentStep.FragmentProfil2;
import com.example.anafuluslender.Register.FragmentStep.FragmentProfil3;
import com.example.anafuluslender.Register.FragmentStep.FragmentProfil4;
import com.example.anafuluslender.Register.FragmentStep.FragmentProfil5;
import com.example.anafuluslender.Register.FragmentStep.FragmentProfil6;
import com.example.anafuluslender.Register.FragmentStep.FragmentProfil7;
import com.example.anafuluslender.Register.FragmentStep.FragmentProfil8;
import com.example.anafuluslender.Register.FragmentStep.FragmentProfil9;
import com.example.anafuluslender.SectionsPageAdapter;

import java.util.ArrayList;

public class RegisterStepThree extends AppCompatActivity {

    private static StackView stackView;
    private static ArrayList list;

    ViewPager mViewPager;
    LinearLayout layoutButton;
//    private ArrayList<StackItems> list;

//    private static final int[] layout = {R.drawable.anafuluslender, R.drawable.ic_google};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_step_three);

        layoutButton = findViewById(R.id.layoutButton);

        //ViewPager
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);


    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentProfil1());
        adapter.addFragment(new FragmentProfil2());
        adapter.addFragment(new FragmentProfil3());
        adapter.addFragment(new FragmentProfil4());
        adapter.addFragment(new FragmentProfil5());
        adapter.addFragment(new FragmentProfil6());
        adapter.addFragment(new FragmentProfil7());
        viewPager.setAdapter(adapter);
        viewPager.setClipToPadding(false);
        viewPager.setPageMargin(20);
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver),
                new IntentFilter("onBottom")
        );
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent != null){
                String isBottom = intent.getStringExtra("isBottom");
                if (isBottom.equals("0")) {
                    layoutButton.setVisibility(View.GONE);
                } else {
                    layoutButton.setVisibility(View.VISIBLE);
                }
                System.out.println("Ceck Bottom" + isBottom);
            }
        }
    };

    @Override
    public void onBackPressed() {

    }
}
