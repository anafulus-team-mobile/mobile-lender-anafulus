package com.example.anafuluslender.Login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anafuluslender.R;
import com.example.anafuluslender.Register.Register;

public class Login extends AppCompatActivity {

    TextView clickTextForgot, clickTextRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        clickTextForgot = findViewById(R.id.clickTextForgot);
        clickTextRegister = findViewById(R.id.clickTextRegister);


        //TextUnderline
        String text = "Klik Disini";
        String text1 = "silahkan daftar Disini";
        SpannableString ss = new SpannableString(text);
        SpannableString ss1 = new SpannableString(text1);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Toast.makeText(Login.this, "di klik", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.white));
            }
        };

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Intent i = new Intent(Login.this, Register.class);
                startActivity(i);
                finish();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.white));
            }
        };
        ss.setSpan(clickableSpan, 5, 11, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        clickTextForgot.setText(ss);
        clickTextForgot.setMovementMethod(LinkMovementMethod.getInstance());
        ss1.setSpan(clickableSpan1, 16, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        clickTextRegister.setText(ss1);
        clickTextRegister.setMovementMethod(LinkMovementMethod.getInstance());


    }
}
