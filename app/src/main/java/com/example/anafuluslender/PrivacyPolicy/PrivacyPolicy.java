package com.example.anafuluslender.PrivacyPolicy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.anafuluslender.R;
import com.example.anafuluslender.Register.Register;

public class PrivacyPolicy extends AppCompatActivity {

    ImageView buttonBack;
    Button buttonBackPrivacyPolicy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privacy_policy);

        buttonBack = findViewById(R.id.buttonBack);
        buttonBackPrivacyPolicy = findViewById(R.id.buttonBackPrivacyPolicy);

        //BUTTON BACK ON TOOLBAR
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PrivacyPolicy.this, Register.class);
                startActivity(i);
                finish();
            }
        });
        //BUTTON BACK PRIVACY POLICY
        buttonBackPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PrivacyPolicy.this, Register.class);
                startActivity(i);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(PrivacyPolicy.this, Register.class);
        startActivity(i);
        finish();
    }
}
